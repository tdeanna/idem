"""Utility functions for the idem-sqlite-demo project"""


async def sqlite_to_dict(hub, ctx, cur):
    """Convert sqlite cursor to a python dictionary.

    Args:

        cur (str):
            Sqlite cursor
    """
    return [dict(row) for row in cur]
