======
Idem 7
======

Idem 7 introduces two major new features. A revamped, and easier to use cli,
and the new `acct` system.

New CLI
=======

The new cli makes calling states and execution execution modules significantly
easier. Now you can just can an sls file directly, or use the existing
sls tree settings. This makes idem work more like a programming language.

.. code-block:: bash

   idem state cloud.sls

   idem exec cmd.run 'ls -l' shell=True

This new simplification should make the use of idem much easier!

The Acct system
===============

The new `acct` system allows for account information to be loaded via plugins
into idem. It allows for plugins to be provided by cloud providers that load
up the api credentials that should be passed through to the cloud providers.

This systems allows for multiple cloud providers, and multiple accounts, to
be targeted simultaneously. So a single execution of idem can orchestrate
setting up and maintaining resources across multiple clouds and apis.

It can have cloud specific login plugins, or it can run from a single, encrypted
file. The single encrypted file can work across multiple cloud providers
and does not require cloud specific plugins.
