def stage(hub, name: str):
    """
    Take the highdata and reconcoile the extend keyword
    """
    high, errors = hub.idem.ccomps.extend.reconcile(hub.idem.RUNS[name]["high"])
    hub.idem.RUNS[name]["high"] = high
    hub.idem.RUNS[name]["errors"] = errors
