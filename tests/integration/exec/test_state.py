import pytest


@pytest.mark.asyncio
async def test_run_sleep(hub):
    """
    Run the example used in the state.run function header
    """
    ret = await hub.exec.state.run(path="time.sleep", duration=0)
    assert ret == {
        "result": True,
        "ret": {},
        "comment": ("Successfully slept for 0 seconds.",),
        "ref": "exec.state.run",
        "test": False,
    }


@pytest.mark.asyncio
async def test_run_configurable(hub):
    """
    Verify that a configurable test state with a "new_state" return gets included in the exec call's `ret`
    """
    ret = await hub.exec.state.run(path="test.present", new_state={"key": "value"})
    assert ret == {
        "result": True,
        "ret": {"key": "value"},
        "comment": None,
        "ref": "exec.state.run",
        "test": False,
    }


def test_run_cli(idem_cli):
    """
    Shell out for the exec call on the cli to verify that it works exactly the same as the pure python call
    """
    ret = idem_cli(
        "exec", "state.run", "test.present", 'new_state={"key": "value"}', acct_data={}
    )
    assert ret["json"] == {
        "result": True,
        "ret": {"key": "value"},
        "comment": None,
        "ref": "exec.state.run",
        "test": False,
    }


def test_run_cli_test(idem_cli):
    """
    Run the configurable state with test=True
    """
    ret = idem_cli("exec", "state.run", "test.present", "test=True", acct_data={})
    assert ret["json"] == {
        "result": True,
        "ret": None,
        "comment": None,
        "ref": "exec.state.run",
        "test": True,
    }
