from pytest_idem.runner import idem_cli


def test_arg_bind_in_json(tests_dir):
    cmd_output = idem_cli(
        "state",
        f"--tree={tests_dir / 'sls/arg_bind'}",
        "arg_bind_json",
    )
    ret = cmd_output["json"]
    assert len(ret) == 3
    assert "test_|-test_run.state_A_|-test_run.state_A_|-present" in ret
    assert "test_|-test_run.state_B_|-state_b_name_|-present" in ret
    resource = ret["test_|-test_run.state_B_|-state_b_name_|-present"]["new_state"]
    assert "idem-test-state_a_name" == resource["key"]
