import pytest_idem.runner as runner
from pytest_idem.runner import idem_cli


RESOURCE_SLS_WITH_CHANGES = """
test_resource:
    test_resource.present:
      - resource_name: test_changes_1
      - changes: {}
      - old_state: {some_property: value}
      - new_state: {some_property: value}
"""

RESOURCE_SLS_WITH_COMPUTED_PARAMS = """
test_resource:
    test_resource.present:
      - resource_name: test_changes_1
      - old_state:
          computed_str: old_value
          dataclass:
            - test: not_changed_1
              dataclass_computed_str: old_value_computed_str
            - test: not_changed_2
              dataclass_computed_str: old_value_computed_str
      - new_state:
          computed_str: new_value
          dataclass:
            - test: not_changed_2
              dataclass_computed_str: new_value_computed_str
            - test: not_changed_1
"""

RESOURCE_SLS_WITH_AMBIGUOUS_COMPUTED_PARAMS_NO_CHANGES = """
test_resource:
    test_resource.present:
      - resource_name: test_changes_1
      - old_state:
          dataclass:
              test_ambiguous_computed: old_value_computed_str
      - new_state:
          dataclass:
              test_ambiguous_computed: new_value_computed_str
"""

RESOURCE_SLS_WITH_AMBIGUOUS_COMPUTED_PARAMS_CHANGES = """
test_resource:
    test_resource.present:
      - resource_name: test_changes_1
      - old_state:
          test_ambiguous_computed: old
          dataclass:
              test_ambiguous_computed: old_value_computed_str
      - new_state:
          test_ambiguous_computed: new
          dataclass:
              test_ambiguous_computed: new_value_computed_str
"""


def test_changes_not_being_recalculated(tests_dir):
    output = execute_sls(RESOURCE_SLS_WITH_CHANGES)

    assert 1 == len(output), len(output)
    tag = "test_resource_|-test_resource_|-test_changes_1_|-present"

    result = output[tag]

    # Result is True
    assert result["result"] is True

    # Check that changes are as set, not recalculated
    assert result["changes"] == {}


def test_changes_with_computed_values(tests_dir):
    output = execute_sls(RESOURCE_SLS_WITH_COMPUTED_PARAMS)

    assert 1 == len(output), len(output)
    tag = "test_resource_|-test_resource_|-test_changes_1_|-present"

    result = output[tag]

    # Result is True
    assert result["result"] is True

    # Check that changes are as set, not recalculated
    assert result["changes"] == {}


def test_changes_with_computed_values_ambiguous_computed_params_no_changes(tests_dir):
    output = execute_sls(RESOURCE_SLS_WITH_AMBIGUOUS_COMPUTED_PARAMS_NO_CHANGES)

    assert 1 == len(output), len(output)
    tag = "test_resource_|-test_resource_|-test_changes_1_|-present"

    result = output[tag]

    # Result is True
    assert result["result"] is True

    # Check there is no changes
    assert result["changes"] == {}


def test_changes_with_computed_values_ambiguous_computed_params_changes(tests_dir):
    output = execute_sls(RESOURCE_SLS_WITH_AMBIGUOUS_COMPUTED_PARAMS_CHANGES)

    assert 1 == len(output), len(output)
    tag = "test_resource_|-test_resource_|-test_changes_1_|-present"

    result = output[tag]

    # Result is True
    assert result["result"] is True

    # Check that changes are set for non-nested test_ambiguous_computed
    assert result["changes"] == {
        "new": {"test_ambiguous_computed": "new"},
        "old": {"test_ambiguous_computed": "old"},
    }


def execute_sls(sls_to_execute):
    acct_data = {"profiles": {"test": {"default": {}}}}
    with runner.named_tempfile(suffix=".sls", delete=True) as fh:
        fh.write_text(sls_to_execute)

        return idem_cli(
            "state",
            fh,
            "--run-name=test",
            check=True,
            acct_data=acct_data,
        ).json
