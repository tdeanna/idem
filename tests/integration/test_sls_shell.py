import json


def test_sls_shell(tree, idem_cli):
    ret = idem_cli(
        "state",
        "simple",
        "--output=state",
        "--sls-sources",
        f"file://{tree}",
        check=True,
    ).stdout
    assert "True" in ret
    assert "Success!" in ret
    assert "Failure!" in ret
    assert "happy" in ret
    assert "sad" in ret


def test_sls_implied_source(code_dir, idem_cli):
    sls = code_dir.joinpath("tests", "sls", "simple.sls")
    ret = idem_cli("state", "--output=state", sls, check=True).stdout
    assert "True" in ret
    assert "Success!" in ret
    assert "Failure!" in ret
    assert "happy" in ret
    assert "sad" in ret


def test_sls_tree(tree, idem_cli):
    ret = idem_cli(
        "state", "simple", "--output=state", "--tree", tree, check=True
    ).stdout
    assert "True" in ret
    assert "Success!" in ret
    assert "Failure!" in ret
    assert "happy" in ret
    assert "sad" in ret


def test_fail(tree, idem_cli):
    ret = idem_cli("state", "--output=state", "nonexistant.sls", tree, check=False)
    assert "Could not find SLS ref 'nonexistant.sls' in sources" in ret.stdout


def test_cli_with_batch_size(code_dir, idem_cli):
    sls = code_dir.joinpath("tests", "sls", "simple.sls")
    ret = idem_cli(
        "state",
        "--output=state",
        "--batch-size=2",
        sls,
        check=True,
    ).stdout
    assert "True" in ret
    assert "Success!" in ret
    assert "Failure!" in ret
    assert "happy" in ret
    assert "sad" in ret


def test_cli_with_batch_size_and_states_with_requisites(code_dir, idem_cli):
    sls = code_dir.joinpath("tests", "sls", "req.sls")
    output = idem_cli(
        "state",
        "--batch-size=1",
        "--test",
        sls,
        check=True,
    ).stdout

    ret = json.loads(output)
    assert ret["test_|-needs_fail_|-needs_fail_|-nop"]["result"] is False
    assert ret["test_|-needs_in_|-needs_in_|-nop"]["__run_num"] == 1
    assert ret["test_|-fails_|-fails_|-fail_without_changes"]["__run_num"] == 2
    assert ret["test_|-needs_fail_|-needs_fail_|-nop"]["__run_num"] == 3
    assert ret["test_|-needed_|-needed_|-nop"]["__run_num"] == 4
    assert ret["test_|-needs_|-needs_|-nop"]["__run_num"] == 5
    # "needed" returned None and needs did not fail to run
    assert ret["test_|-needs_|-needs_|-nop"]["result"] is None
