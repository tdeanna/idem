test_1:
  reconcile.is_pending.resource_with_success_and_always_pending.present:
    - number_of_reruns: 6

test_2:
  reconcile.is_pending.resource_with_failure_and_always_pending.present:
    - number_of_reruns: 6

test_3:
  reconcile.is_pending.resource_with_pending_and_overridden_empty_changes.present:
    - relevant_prop: relevant_value
