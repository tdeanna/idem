# when create_before_destroy is false

create_scenario:
  test.present:
    - resource_id: idem-test-1
    - new_state:
        key: value
        name: test
        resource_id: idem-test-1
    - result: true
    - recreate_on_update:
        create_before_destroy: false
