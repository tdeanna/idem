service1:
  sls.run:
    - sls_sources:
      - sls.include_file_with_duplicate_ids_1
      - sls.include_file_with_duplicate_ids_2
    - params:
        - params.file1
    - db_parameter_group_name: service1_db_parameter_group


first thing:
  test.succeed_with_changes

multi_result_new_state_1:
  test.present:
    - result: True
    - changes: 'skip'
    - new_state:
        "resource_id_1": "resource-1"
        "name": "subnet1"
        "resource_id_2": "resource-2"
        "resource_id_3": "resource-3"
