The Org policy:
  test.state.present:
  - is_enabled: {{ params.get('is_enabled', false)  }}
  - description: SCP tags policy
  - policy_type: TAG_POLICY
