import copy
from typing import Any
from typing import Dict

__func_alias__ = {"list_": "list"}
__contracts__ = ["auto_state", "soft_fail"]


def __init__(hub):
    hub.exec.tests.auto.ITEMS = None


def get(hub, ctx, name: str, resource_id: str = None, **kwargs) -> Dict[str, Any]:
    result = dict(comment=[], result=True, ret=None)
    result["ret"] = copy.deepcopy(hub.exec.tests.auto.ITEMS)
    return result


def list_(hub, ctx) -> Dict[str, Any]:
    return dict(comment=[], result=True, ret=[hub.exec.tests.auto.ITEMS])


def create(hub, ctx, name: str, kw1=None, **kwargs) -> Dict[str, Any]:
    result = dict(comment=[], result=True, ret=None)
    hub.exec.tests.auto.ITEMS = {
        "name": name,
        "resource_id": name,
        "kw1": kw1,
        **kwargs,
    }

    result["ret"] = copy.deepcopy(hub.exec.tests.auto.ITEMS)
    result["comment"] = f"Created '{name}'"
    return result


def update(hub, ctx, name: str, resource_id: str, **kwargs) -> Dict[str, Any]:
    result = dict(comment=[], result=True, ret=None)
    d = hub.exec.tests.auto.ITEMS
    d.update(kwargs)
    hub.exec.tests.auto.ITEMS = d

    result["comment"] = f"Updated '{name}'"
    return result


def delete(hub, ctx, name: str, resource_id: str = None, **kwargs) -> Dict[str, Any]:
    result = dict(comment=[], result=True, ret=None)
    hub.exec.tests.auto.ITEMS = None
    result["comment"] = f"Deleted '{name}'"
    return result
