__contracts__ = ["resource"]


def present(
    hub,
    ctx,
    name: str,
    resource_id: ("alias=id", str) = None,
):
    ret = {
        "name": name,
        "old_state": None,
        "new_state": {"resource_id": resource_id},
        "result": True,
        "comment": None,
    }
    return ret


def absent(
    hub,
    ctx,
    name: str,
    resource_id: ("alias=id", str) = None,
):
    ret = {
        "name": name,
        "old_state": None,
        "new_state": {"resource_id": resource_id},
        "result": True,
        "comment": None,
    }
    return ret


async def describe(hub, ctx):
    ret = {}
    return ret
