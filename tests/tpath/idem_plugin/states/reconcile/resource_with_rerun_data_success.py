"""The rerun data is not cleared but the result is True always. Reconciler will stop it after 3 runs"""
__contracts__ = ["resource"]
__reconcile_wait__ = {"static": {"wait_in_seconds": 1}}


def present(hub, ctx, name: str, number_of_reruns: int = 0):
    """
    Returns success in state execution
    """
    execution_count = (
        ctx.get("rerun_data", {}).get("execution_count", 0)
        if ctx.get("rerun_data")
        else 0
    )
    execution_count += 1

    return {
        "name": name,
        "result": True,
        # The following will always add rerun_data
        # expectation is it should terminate after --max-rerun-count
        "rerun_data": {"execution_count": execution_count},
        "comment": "test",
        "old_state": {},
        "new_state": {"number_of_reruns": number_of_reruns},
    }


def absent(
    hub,
    ctx,
    name: str,
):
    return {
        "name": name,
    }


async def describe(hub, ctx):
    return {"test": {"resource_with_rerun_data_success.present": []}}
